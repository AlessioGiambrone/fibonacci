FROM node:10

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin
EXPOSE 3000

COPY . /opt/

WORKDIR /opt/

RUN npm install -g

CMD ["./index.js"]

