# Fibonacci REST API

[![Docker Repository on Quay](https://quay.io/repository/alessiogiambrone/fibonacci/status "Docker Repository on Quay")](https://quay.io/repository/alessiogiambrone/fibonacci)

Compute Fibonacci numbers.

## Install and run

```bash
npm install -g
./index.js
```

Now the server is ready:

```bash
curl 'localhost:3000/fibonacci?n=6
```

## Docker

Building the image:

```bash
docker build -t fibonacci .
```

Use it, in this case we'll bind the container port to 9090:

```bash
docker run --rm -p 9090:3000 fibonacci
# call it
curl 'localhost:9090/fibonacci?n=6
```

## Test

Just use

```bash
npm test
```
