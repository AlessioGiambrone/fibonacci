var fibo_lib = require('../lib/fibonacci.js');

var assert = require('assert');
describe('Fibonacci', function() {
  describe('fibonacci', function() {
    it('Should return Fibonacci series for n=6', function() {
      assert.deepEqual(fibo_lib.fibonacci(6), [0,1,1,2,3,5,8]);
    });
    it('Should return empty array if not NaN', function() {
      assert.deepEqual(fibo_lib.fibonacci('ciao'), []);
    });
    it('Should return empty array if undefined', function() {
      assert.deepEqual(fibo_lib.fibonacci(), []);
    });
  })
  describe('fibonacci_rest', function() {
    it('Should return Fibonacci series for n=6 without leading 0', function() {
      assert.deepEqual(fibo_lib.fibonacci_rest(6), [1,1,2,3,5,8]);
    });
    it('Should return nothing if n=0', function() {
      assert.deepEqual(fibo_lib.fibonacci_rest(0), []);
    });
    it('Should return  [1] if n=1', function() {
      assert.deepEqual(fibo_lib.fibonacci_rest(1), [1]);
    });
  })
});
