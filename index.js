#!/usr/bin/env node

var fib_lib = require('./lib/fibonacci.js');
var express = require('express');
var app = express();


app.get('/fibonacci', function (req, res) {
  var n = req.query.n;
  res.end( JSON.stringify( fib_lib.fibonacci_rest(n)) );
})

var server = app.listen(3000, function () {
  var host = server.address().address
  var port = server.address().port
  console.log("Fibonacci app listening at http://%s:%s", host, port)
})
