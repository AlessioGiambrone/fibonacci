module.exports = {

  fibonacci: function(n){
    if (n === undefined | n <= 0 | isNaN(n)){
      return [];
    }
    else if (n <= 1) {
      return [0, 1];
    } 
    else {
      var s = this.fibonacci(n - 1);
      s.push(s[s.length - 1] + s[s.length - 2]);
      return s;
    }
  },

  // Fibonacci without the leading zero
  fibonacci_rest: function(n){
    var fib = this.fibonacci(n);
    fib.shift();
    return fib;
  }

}
